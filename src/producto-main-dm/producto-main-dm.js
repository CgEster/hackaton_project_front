import {
    LitElement,
    html
} from 'lit-element';

class ProductoMainDM extends LitElement {
    static get properties() {
        return {
            products: {
                type: Array
            },
            getProducts: {
              type: Boolean
            },
            newProduct: {
              type: Object
            },
            updateProduct: {
              type: Object
            },
            deleteProduct: {
              type: Object
            },
            deleteProductId: {
              type: String
            }
        };
    }

    constructor() {
        super();

        this.products = [];
        this.newProduct = {}
        this.updateProduct = {}
        this.deleteProduct = {}
        this.deleteProductId = ""

        this.getProductsData();

    }

    updated(changedProperties) {
        console.log("updated");
        if (changedProperties.has("products")) {
            this.getProducts = false;
            console.log("Ha cambiado el valor de la propiedad products");
            this.dispatchEvent(
                new CustomEvent("products-data-updated", {
                    detail: {
                        products: this.products
                    }
                }));
        }
        if (changedProperties.has("getProducts")) {
            console.log("Ha cambiado el valor de la propiedad newProduct");
            this.getProductsData();
        }
        if (changedProperties.has("newProduct")) {
            console.log("Ha cambiado el valor de la propiedad newProduct");
            this.postProduct(this.newProduct);
        }

        if (changedProperties.has("deleteProductId")) {
            console.log("Ha cambiado el valor de la propiedad deleteProduct");
            this.deleteProductID(this.deleteProductId);
        }

        if (changedProperties.has("updateProduct")) {
            console.log("Ha cambiado el valor de la propiedad updateProduct");
            this.putProduct(this.updateProduct);
        }
    }

    getProductsData() {
        console.log("getProductsData");
        console.log("Obteniendo datos de productos");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log("Respuesta del servidor");
                console.log(APIResponse);

                this.products = APIResponse;

            }
        };

        xhr.open("GET", "http://localhost:8081/apitechu/v2/products");
        xhr.send();
        console.log("Fin de getProductsData");
    }

    postProduct(producto) {
        if(Object.keys(producto).length != 0){
          console.log("Nuevo postProducto");
          console.log(JSON.stringify(producto))
          let xhr = new XMLHttpRequest();
          xhr.open("POST", "http://localhost:8081/apitechu/v2/products");
          xhr.setRequestHeader("Content-type", "application/json");
          xhr.send(JSON.stringify(producto));
        }
    }

    deleteProductID(productId) {
        if(productId != ""){
          console.log("deleteProduct");
          console.log("Vamos a borrar el producto de identificador " + productId);
          let xhr = new XMLHttpRequest();
          xhr.open("DELETE", "http://localhost:8081/apitechu/v2/products/" + this.deleteProductId);
          xhr.send();
        }
    }

    putProduct(producto) {
        if(Object.keys(producto).length != 0){
          console.log("putProduct");
          console.log(JSON.stringify(producto))
          let xhr = new XMLHttpRequest();
          xhr.open("PUT", "http://localhost:8081/apitechu/v2/products/" + producto.id);
          xhr.setRequestHeader("Content-type", "application/json");
          xhr.send(JSON.stringify(producto));
        }
    }

}

customElements.define('producto-main-dm', ProductoMainDM);
