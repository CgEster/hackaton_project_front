import { LitElement, html } from 'lit-element';
import '../producto-header/producto-header.js';
import '../producto-main/producto-main.js';
import '../producto-footer/producto-footer.js';
import '../producto-sidebar/producto-sidebar.js';
import '../producto-stats/producto-stats.js';

class ProductoApp extends LitElement {

        static get properties() {
                return {
                        products: {type: Array}
                };
        }

        render() {
                return html`
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                        <producto-header></producto-header>
                        <div class="row">
                                <producto-sidebar class="col-2"
                                        @updated-max-years-filter="${this.newMaxYearsInCompanyFilter}"
                                        @new-product="${this.newProduct}"
                                >
                                </producto-sidebar>
                                <producto-main @updated-products="${this.updatedProducts}" class="col-10"></producto-main>
                        </div>
                        <producto-footer></producto-footer>
                        <producto-stats @updated-products-stats="${this.updatedProductsStats}"></producto-stats>
                `;
                }

        updated(changedProperties){
                console.log("updated en producto-app");
                console.log(this.products);

                if(changedProperties.has("products")){
                        console.log("Ha cambiado el valor de la propiedad products en producto-app");
                        console.log(this.products);
                        this.shadowRoot.querySelector("producto-stats").products = this.products;
                }
        }

        newMaxYearsInCompanyFilter(e){
                console.log("newMaxYearsInCompanyFilter");
                console.log("nuevo filtro es " + e.detail.maxYearsInCompany);
                this.shadowRoot.querySelector("producto-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany;
        }

        updatedProducts(e) {
                console.log("updatedProducts en product-app");
                console.log(e.detail.products);
                this.products = e.detail.products;
        }

        newProduct(e) {
                console.log("newProduct en producto-app");
                this.shadowRoot.querySelector("producto-main").showProductForm = true;
        }

        updatedProductsStats(e) {
                console.log("updatedProductsStats en producto-app");
                this.shadowRoot.querySelector("producto-sidebar").productsStats = e.detail.productsStats;
                this.shadowRoot.querySelector("producto-main").maxYearsInCompanyFilter = e.detail.productsStats.maxYearsInCompany;
        }
}

customElements.define('producto-app', ProductoApp);
