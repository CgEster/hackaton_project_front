import { LitElement, html } from 'lit-element';

class ProductoFooter extends LitElement{
    render() {
        return html`
                <div class="row justify-content-end">
                <h5 style="text-align:right">Grupo 3 Hackaton Tech U! 2020 ©</h5>
                </div>
        `;
        }
}

customElements.define('producto-footer', ProductoFooter);
