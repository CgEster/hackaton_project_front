 import { LitElement, html } from 'lit-element';

class ProductoSidebar extends LitElement {

        static get properties() {
                return {
                        productsStats: {type: Array}
                };
        }

        constructor() {
                super();

                this.productsStats = {};
        }

        render() {
                return html`
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                <aside>
                        <section>
                                <div>
                                        Hay <span class="badge badge-pill badge-primary">${this.productsStats.numberOfProducts}</span> PRODUCTOS
                                </div>
                                <div>
                                        <input type="range" min="0" max="${this.productsStats.maxYearsInCompany}"
                                        step="1"
                                        value="${this.productsStats.maxYearsInCompany}"
                                        @input="${this.updatemaxYearsInCompanyFilter}"
                                        />
                                </div>
                                <div>
                                  <p>Máximo (${this.productsStats.maxYearsInCompany} meses)</p>
                                </div>
                                <div class="mt-5">
                                        <button class="w-100 btn btn-info" style="font-size: 20px;  background-color:#1464A5"
                                        @click="${this.newProduct}"><strong>Añadir producto</strong></button>
                                </div>

                        </section>
                </aside>
                `;
        }

        updatemaxYearsInCompanyFilter(e){
                console.log("updatemaxYearsInCompanyFilter");
                console.log("el range vale: " + e.target.value);

                this.dispatchEvent(new CustomEvent("updated-max-years-filter",
                        {
                                detail: {
                                        maxYearsInCompany: e.target.value
                                }
                        }
                )
                );
        }

        newProduct(e) {
                console.log("newProduct en producto-sidebar");
                console.log("Se va a crear un producto nuevo");

                this.dispatchEvent(new CustomEvent("new-product", {}));
        }
}

customElements.define('producto-sidebar', ProductoSidebar)
