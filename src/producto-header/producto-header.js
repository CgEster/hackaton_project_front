import { LitElement, html } from 'lit-element';

class ProductoHeader extends LitElement{
    render() {
        return html`
        <div style=" background-color:#d4edfc; vertical-align:middle; padding:10px;
        padding-bottom: 1px; margin-bottom:10px;" class="p-10">
          <h2 style="text-align:center;"><img src='./img/bbva_logo.png' width="15%" height="15%"><p>Productos de Financiación</p></h2>
        </div>
        `;
        }
}

customElements.define('producto-header', ProductoHeader);
