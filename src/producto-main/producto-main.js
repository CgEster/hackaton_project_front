import { LitElement, html } from 'lit-element';
import '../producto-ficha-listado/producto-ficha-listado.js';
import '../producto-form/producto-form.js';
import '../producto-main-dm/producto-main-dm.js';

class ProductoMain extends LitElement{

        static get properties() {
                return {
                        products: {type: Array},
                        showProductForm: {type: Boolean},
                        maxYearsInCompanyFilter: {type: Number}
                };
        }

        constructor() {
                super();

                this.products = [];
                this.showProductForm = false;
                this.maxYearsInCompanyFilter = 0;
        }

        render() {
                return html`
                        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                        <div class="ml-3" id="productsList">
                                <div class="row ">
                                ${this.products.filter(
                                        product => product.period <= this.maxYearsInCompanyFilter
                                ).map(
                                        product => html`<producto-ficha-listado class="col-3 mb-4"
                                                id="${product.id}"
                                                name="${product.name}"
                                                description="${product.description}"
                                                period="${product.period}"
                                                photo="${product.photo}"
                                                segment="${product.segment}"
                                                numUsers="${product.numUsers}"
                                                @delete-product="${this.deleteProduct}"
                                                @info-product="${this.infoProduct}"
                                                >
                                        </producto-ficha-listado>`
                                )}
                                </div>
                        </div>
                        <div class="row">
                               <producto-form id="productoForm" class=""
                                   @producto-form-close="${this.productFormClose}"
                                   @producto-form-store="${this.productFormStore}"
                               >
                               </producto-form>
                        </div>
                        <producto-main-dm @products-data-updated="${this.productsDataUpdated}"></producto-main-dm>
                `;
                }

                productsDataUpdated(e){
                        console.log("productsDataUpdated");
                        console.log(e.detail.products);
                        this.products = e.detail.products;
                }

                updated(changedProperties){
                        console.log("updated");

                        if (changedProperties.has("showProductForm"))
                        {
                                console.log("Ha cambiado el valor de la propiedad en producto-main");

                                if (this.showProductForm === true) {
                                        this.showProductFormData();
                                } else {
                                        this.showProductList();
                                }
                        }

                        if (changedProperties.has("products")) {
                                console.log("Ha cambiado el valor de la propiedad products en producto-main");

                                this.dispatchEvent(new CustomEvent("updated-products", {
                                        detail: {
                                                products: this.products
                                        }
                                }
                                ));
                        }

                        if (changedProperties.has("maxYearsInCompanyFilter")) {
                                console.log("Ha cambiado el valor de la propiedad en producto-main");
                                console.log("Se van a mostrar los productos cuya antiguedad sea " + this.period +  " meses");
                        }
                }

                productFormStore(e) {
                        console.log("productFormStore");
                        console.log("Se va almacenar un producto");

                        console.log(e.detail);


                        if (e.detail.editingProduct === true)
                        {
                                console.log("Se va actualizar el producto de id " + e.detail.product.id);

                                this.products = this.products.map (
                                        product => product.id === e.detail.product.id
                                                ? product = e.detail.product : product
                                )
                                this.shadowRoot.querySelector("producto-main-dm").updateProduct = e.detail.product;

                        }
                        else
                        {
                                console.log("Se va almacenar un producto nuevo");
                                //this.people.push(e.detail.person);

                                this.shadowRoot.querySelector("producto-main-dm").newProduct = e.detail.product;
                                this.products = [...this.products, e.detail.product];
                        }

                        console.log("producto almacenado");


                        this.shadowRoot.querySelector("producto-main-dm").getProducts = true;

                        this.showProductForm = false;
                }

                productFormClose(e) {
                        console.log("ProductFormClose");
                        console.log("Se ha cerrado el formulario del producto");

                        this.shadowRoot.querySelector("producto-main-dm").getProducts = true;

                        this.showProductForm = false;
                }

                showProductFormData() {
                        console.log("showProductFormData");
                        console.log("Mostrando el formulario de producto");
                        this.shadowRoot.getElementById("productsList").classList.add("d-none");
                        this.shadowRoot.getElementById("productoForm").classList.remove("d-none");
                }

                showProductList() {
                        console.log("showProductList");
                        console.log("Mostrando listado de productos");
                        this.shadowRoot.getElementById("productsList").classList.remove("d-none");
                        this.shadowRoot.getElementById("productoForm").classList.add("d-none");
                }

                deleteProduct(e) {
                        console.log("deleteProduct en producto-main");
                        console.log("Se va a borrar el producto de id " + e.detail.id);
                        this.products = this.products.filter (
                                product => product.id != e.detail.id
                        );
                        this.shadowRoot.querySelector("producto-main-dm").deleteProductId = e.detail.id;

                }

                infoProduct(e) {
                        console.log("infoProduct en producto-main");
                        console.log("Se ha pedido más información del producto de id " + e.detail.id);

                        let chosenProduct = this.products.filter (
                                product => product.id === e.detail.id
                        );

                        console.log(chosenProduct);
                        this.shadowRoot.getElementById("productoForm").product = chosenProduct[0];
                        this.shadowRoot.getElementById("productoForm").editingProduct = true;
                        this.showProductForm = true;
                }
}

customElements.define('producto-main', ProductoMain);
