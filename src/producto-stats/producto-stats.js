import { LitElement, html } from 'lit-element';

class ProductoStats extends LitElement{

        static get properties() {
                return {
                        products: {type: Array}
                };
        }

        constructor() {
                super();

                this.products = [];
        }

        updated(changedProperties){
                console.log("updated en producto-stats");
                if(changedProperties.has("products")){
                        console.log("Ha cambiado el valor de la propiedad products en producto-stats");
                        let productsStats = this.gatherProductsArrayInfo(this.products);

                        this.dispatchEvent(new CustomEvent("updated-products-stats", {
                                detail: {
                                        productsStats: productsStats
                                }
                        }
                        ));
                }
        }

        gatherProductsArrayInfo(products){
                console.log("gatherProductsArrayInfo");
                console.log(this.products);

                let productsStats = {};

                productsStats.numberOfProducts = this.products.length;

                let maxYearsInCompany = 0;

                this.products.forEach(product => {
                        if(product.period > maxYearsInCompany){
                                maxYearsInCompany = product.period
                        }
                });

                console.log(this.products)

                console.log("maxYearsInCompany es " + maxYearsInCompany);
                productsStats.maxYearsInCompany = maxYearsInCompany;

                return productsStats;
        }
}

customElements.define('producto-stats', ProductoStats);
