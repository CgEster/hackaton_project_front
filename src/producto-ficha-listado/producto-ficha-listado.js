import { LitElement, html } from 'lit-element';

class ProductoFichaListado extends LitElement{

    static get properties() {
            return {
                    id: {type: String},
                    name: {type: String},
                    description: {type: String},
                    period: {type: Number},
                    photo: {type: String},
                    segment: {type: String},
                    numUsers: {type: Number}
            }
    }

    render() {
        return html`
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                <div class="card h-100">
                        <img src="${this.photo}" alt="${this.name}" height="150" width="100" class="card-img-top">
                        <div class="card-body">
                                <h5 class="card-title">${this.name}</h5>
                                <p class="card-text">${this.description}</p>
                                <p class="card-text">${this.period} meses</p>
                                <p class="card-text">${this.segment}</p>
                                <p class="card-text">Este producto tiene ${this.numUsers} ${this.numUsers === 1 ? "cliente" : "clientes"}</p>
                        </div>
                        <div class="card-footer">
                                <button @click="${this.deleteProduct}" class="btn btn-danger col-5"> <strong>Borrar</strong> </button>
                                <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"  style="background-color:#1464A5"> <strong>Info</strong> </button>
                        </div>
                </div>
        `;
        }

        deleteProduct(e){
                console.log("deleteProduct en producto-ficha-listado");
                console.log("Se va a borrar el producto de id " + this.id);
                this.dispatchEvent(
                        new CustomEvent ("delete-product", {
                                detail: {
                                        id: this.id
                                }
                        })
                );
        }

        moreInfo(e){
                console.log("moreInfo");
                console.log("Se ha pedido información del producto de id " + this.id);
                this.dispatchEvent(
                        new CustomEvent ("info-product",  {
                                detail: {
                                        id: this.id
                                }
                        })
                );
        }
}

customElements.define('producto-ficha-listado', ProductoFichaListado);
