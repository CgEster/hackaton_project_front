import { LitElement, html } from 'lit-element';

class ProductoForm extends LitElement{

        static get properties() {
                return {
                        product: {type: Object},
                        editingProduct: {type: Boolean}
                }
        }

        constructor() {
                super();

                this.resetFormData();
        }


    render() {
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <div>
                <form>
                        <div class="form-group">
                                <img src="${this.product.photo}">
                        </div>
                        <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" id="nameProduct" class="form-control" placeholder="nombre"
                                @input="${this.updateName}"
                                .value="${this.product.name}"
                                />
                        </div>
                        <div class="form-group">
                                <label>Descripción</label>
                                <input type="text" id="descProduct" class="form-control" placeholder="descripcion"
                                @input="${this.updateDescription}"
                                .value="${this.product.description}"
                                />
                        </div>
                        <div class="form-group">
                                <label>Plazo (meses)</label>
                                <input type="text" id="periodProduct" class="form-control" placeholder="X meses"
                                @input="${this.updatePeriod}"
                                .value="${this.product.period}"
                                />
                        </div>
                        <div class="form-group">
                                <label>Segmento</label>

                                <select class="custom-select" id="segmentProduct"
                                             @input="${this.updateSegment}"
                                             .value="${this.product.segment}">
                                      <option value="default">Selecciona el segmento...</option>
                                      <option value="Corporaciones">Corporaciones</option>
                                      <option value="Empresas">Empresas</option>
                                      <option value="Instituciones">Instituciones</option>
                                      <option value="Particulares">Particulares</option>
                                      <option value="Pymes">Pymes</option>
                                </select>
                        </div>
                        ${this.product.numUsers > 0 ? html`
                                <h4>Clientes</h4>
                                <div class="row form-group px-3">
                                    <table class="table table-bordered">
                                        <thead class="thead-dark">
                                           <tr>
                                                <th style="background-color:#1464A5" scope="col">Nombre</th>
                                                <th style="background-color:#1464A5" scope="col">Segmento</th>
                                                <th style="background-color:#1464A5" scope="col">Interés</th>
                                                <th style="background-color:#1464A5" scope="col">Fecha de vencimiento</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                    ${this.product.users.map(
                                            user => html`
                                                      <tr>
                                                              <td>${user.name}</td>
                                                              <td>${user.segment}</td>
                                                              <td>${user.interest}</td>
                                                              <td>${user.expiryDate}</td>
                                                      </tr>
                                                `
                                            )}
                                            </tbody>
                                      </table>
                                </div>
                            ` :
                            html`
                            <div class="alert alert-warning" role="alert">
                                Este producto no ha sido contratado por ningún cliente.</div>
                                `}


                        <button class="btn btn-primary"  style="background-color:#1464A5" @click="${this.goBack}"><strong>Atras</strong></button>
                        <button class="btn btn-success" @click="${this.storeProduct}"><strong>Guardar</strong></button>
                </form>
        </div>
        `;
        }

        updateId(e){
                console.log("udpateId");
                console.log("actualizando la propiedad id con el valor " + e.target.value);
                this.product.id = e.target.value;
        }

        updateName(e){
                console.log("udpateName");
                console.log("actualizando la propiedad name con el valor " + e.target.value);
                this.product.name = e.target.value;
        }

        updateDescription(e){
                console.log("udpateDescription");
                console.log("actualizando la propiedad description con el valor " + e.target.value);
                this.product.description = e.target.value;
        }

        updatePeriod(e){
                console.log("udpatePeriod");
                console.log("actualizando la propiedad Period con el valor " + e.target.value);
                this.product.period = e.target.value;
        }

        updateSegment(e){
                console.log("udpateSegment");
                console.log("actualizando la propiedad Segment con el valor " + e.target.value);
                this.product.segment = e.target.value;
        }

        goBack(e) {
                console.log("goBack");
                e.preventDefault();

                this.dispatchEvent( new CustomEvent("producto-form-close", {}) );

                this.resetFormData();
        }

        storeProduct(e) {
                console.log("storeProduct");
                e.preventDefault();

                console.log("La propiedad id: " + this.product.id);
                console.log("La propiedad name: " + this.product.name);
                console.log("La propiedad description: " + this.product.description);
                console.log("La propiedad period: " + this.product.period);
                console.log("La propiedad segment: " + this.product.segment);

                this.dispatchEvent(new CustomEvent("producto-form-store", {
                        detail: {
                                product: {
                                        id: this.product.id,
                                        name: this.product.name,
                                        description: this.product.description,
                                        period: parseInt(this.product.period),
                                        photo: this.product.photo,
                                        segment: this.product.segment,
                                },
                                editingProduct: this.editingProduct
                        }
                }
                ));
        }

        resetFormData() {
                console.log("resetFormData1");
                this.product = {};
                this.product.id = ""
                this.product.name = ""
                this.product.description = ""
                this.product.period = ""
                this.product.photo = "./img/persona.jpg"
                this.product.segment = ""
                this.product.users = []

                console.log("editingProduct");
                this.editingProduct = false;
        }
}

customElements.define('producto-form', ProductoForm);
